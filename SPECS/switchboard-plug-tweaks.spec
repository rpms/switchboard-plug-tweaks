## START: Set by rpmautospec
## (rpmautospec version 0.2.6)
%define autorelease(e:s:pb:) %{?-p:0.}%{lua:
    release_number = 1;
    base_release_number = tonumber(rpm.expand("%{?-b*}%{!?-b:1}"));
    print(release_number + base_release_number - 1);
}%{?-e:.%{-e*}}%{?-s:.%{-s*}}%{?dist}
## END: Set by rpmautospec

%global __provides_exclude_from ^%{_libdir}/switchboard/.*\\.so$

%global plug_type personal
%global plug_name pantheon-tweaks

Name:           switchboard-plug-tweaks
Summary:        Switchboard Tweaks Plug
Version:        1.0.4
Release:        %autorelease
License:        GPLv3+

URL:            https://github.com/pantheon-tweaks/pantheon-tweaks
Source0:        %{url}/archive/%{version}/%{plug_name}-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  gettext
BuildRequires:  libappstream-glib
BuildRequires:  meson
BuildRequires:  vala

BuildRequires:  pkgconfig(gee-0.8)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(granite) >= 6.0.0
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(switchboard-2.0)

Requires:       switchboard%{?_isa}

Requires:       hicolor-icon-theme


%description
A system settings panel for the Pantheon Desktop Environment that lets
you easily and safely customise your desktop's appearance.

%description -l de
Ein Systemeinstellungsmodul für die Arbeitsumgebung »Pantheon«, mit die
Erscheinung der Arbeitsumgebung sicher und einfach angepasst werden kann.

%description -l fr
Un panneau de configuration système pour le bureau Pantheon qui vous permet
de personnaliser facilement et en toute sécurité l’apparence de votre bureau.

%description -l ja
簡単かつ安全にデスクトップの外観をカスタマイズできる、Pantheon デスクトップ向
けのシステム設定パネルです。

%description -l pt
Um painel de definições do sistema para o ambiente de trabalho Pantheon que
permite personalizar com facilidade e segurança a aparência do seu ambiente
de trabalho.


%prep
%autosetup -n %{plug_name}-%{version}


%build
%meson
%meson_build


%install
%meson_install

%find_lang %{plug_name}-plug

# remove the specified stock icon from appdata (invalid in libappstream-glib)
sed -i '/icon type="stock"/d' %{buildroot}/%{_datadir}/metainfo/%{plug_name}.appdata.xml

%check
appstream-util validate-relax --nonet \
    %{buildroot}/%{_datadir}/metainfo/%{plug_name}.appdata.xml


%files -f %{plug_name}-plug.lang
%license COPYING
%doc README.md
%doc AUTHORS
%doc CONTRIBUTORS

%{_libdir}/switchboard/%{plug_type}/lib%{plug_name}.so

%{_datadir}/metainfo/%{plug_name}.appdata.xml
%{_datadir}/icons/hicolor/*/categories/preferences-*.svg


%changelog
* Wed May 25 2022 Christopher Crouse <mail@amz-x.com> 1.0.4-1
- Update to version 1.0.4; Fixes RHBZ#2089865

* Mon May 02 2022 Christopher Crouse <mail@amz-x.com> 1.0.3-4
- Fixes RHBZ#2080359

* Sat Jan 22 2022 Fedora Release Engineering <releng@fedoraproject.org> 1.0.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Mon Jan 17 2022 Christopher Crouse <mail@amz-x.com> 1.0.3-2
- Updated readme; Initial import fixes RHBZ#2034532

* Sun Jan 16 2022 Christopher Crouse <mail@amz-x.com> 1.0.3-1
- initial import (#2034532)
